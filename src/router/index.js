import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/HomePage'
import Contact from '@/components/Contact'
import AboutUs from '@/components/AboutUs'
import SignUp from '../components/Auth/SignUp'
import Login from '../components/Auth/Login'
import UserBaseView from '../components/UserBaseView'
import DataTable from '../components/Auth/DataTable'
import AdminBaseView from '../components/Auth/AdminBaseView'
import Item from '../components/Item'
import Billing from '@/components/Billing'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'UserBaseView',
      component: UserBaseView,
      children: [
          {
              path: '',
              name: 'Home',
              component: Home
          },
          {
              path: 'contact',
              name: 'Contact',
              component: Contact
          },
          {
              path: 'about',
              name: 'About-us',
              component: AboutUs
          },
          {
              path: 'sign-up',
              name: 'Sign-Up',
              component: SignUp
          },
          {
              path: 'log-in',
              name: 'Log-in',
              component: Login
          },
          {
              path: 'item-detail/:detail',
              name: 'Item-detail',
              component: Item,
              props: true
          },
          {
              path: 'billing',
              name: 'Billing',
              component: Billing,
          }
      ]

    },
    {
        path: '/admin',
        name: 'AdminBaseView',
        component: AdminBaseView,
        children: [
            {
              path:  'data-table',
              name:  'data-table',
              component: DataTable
            }
        ]
    }

  ]
})
