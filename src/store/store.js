import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        user: null,
        auth_token: null,
        cartItems: [],
        sessionExpired: null,
    },
    getters: {
       noOfCartItems: state => {
          let total = 0;
          state.cartItems.forEach(item => {
              total = total + item.addedQuantity;
          });
          if(total) {
              return total;
          }
           return 0;
       },
        noOfItem: (state) => (id) => {
          let addedQuantatiy = 0;
             state.cartItems.forEach(item => {
                   if(item.id === id) {
                       addedQuantatiy = item.addedQuantity;
                   }
              });
            return addedQuantatiy;
        },
        userDetail: state => {
           return state.user;
        }
    },
    mutations: {
        emptyCart:state => {
          state.cartItems = [];
        },
        setAuthToken (state,payload){
            state.auth_token = payload;
        },
        setUser (state,payload) {
            state.user = payload;
        },
        addToCart:(state, payload) => {
            payload.addedQuantity = 1;
            state.cartItems.push(payload);
        },
        incrementItem: (state, payload) => {
            let i = null;
            state.cartItems.forEach((item,index) =>{
                   if(item.id == payload.id){
                       i = index;
                       item.addedQuantity = item.addedQuantity + 1;
                       return false;
                   }
            });
           let item = state.cartItems[i];
            Vue.set(state.cartItems, i, item);
        },
        updateUser: (state,payload) => {
            state.user = payload;
        },
        decrementItem: (state, payload) => {
            let i = null;
           state.cartItems.forEach((item,index) =>{
                if(item.id == payload.id){
                    i = index;
                    if(item.addedQuantity) {
                        item.addedQuantity = item.addedQuantity - 1;
                        return false;
                    }
                    return false;
                }
            });
            let item = state.cartItems[i];
            if(item.addedQuantity === 0) {
                state.cartItems.splice(i, 1);
            }
            else {
                Vue.set(state.cartItems, i, item);
            }
        },
        expireSession: (state, payload) => {
            console.log('payload',payload);
            state.sessionExpired = payload;
            // localStorage.removeItem('authToken');
            // localStorage.removeItem('userDetail');
        }
    },
    actions: {
        expireSession: (context) => {
            context.commit('expireSession',true);
            setTimeout(function () {
                context.commit('expireSession', false);
            }, 6000);
        }
    }
});