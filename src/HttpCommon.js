import axios from 'axios'
import {store} from './store/store'

export const HTTP = axios.create({
    baseURL: 'http://jwtapi.localhost:8080/api',
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
    }
});
console.log(store.state.auth_token);
export const instance = axios.create({
    baseURL: 'http://jwtapi.localhost:8080/api',
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${ store.state.auth_token }`,
    }
});

